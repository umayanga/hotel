<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Room extends Model {

    //
    protected $table = 'room';
    protected $fillable = [
        //
        'room_code',
        'roomState',
        'hotelId',
        'roomTypeId'
    ];

}
