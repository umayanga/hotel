<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;

use DB;
use Auth;
use App\Models\SystemUser;
use App\Models\Room;
use App\Models\CalendarEvent;
use App\Models\Hotel;
use App\Models\RoomType;
use App\Models\Reservation;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use MaddHatter\LaravelFullcalendar\Event;

class RoomMapController extends Controller {

    /**
     * @var CalendarEvent
     */
    private $calendarEvent;

    /**
     * @param CalendarEvent $calendarEvent
     */
    public function __construct(CalendarEvent $calendarEvent) {
        $this->calendarEvent = $calendarEvent;
        // dd($this);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $user_id = Auth::user()->id;


        $hotel_id = DB::select(DB::raw("SELECT hotelId FROM user_role WHERE userId ='+$user_id+';"));

        if ($hotel_id != []) {
            $h_id = $hotel_id[0]->hotelId;
            // dd($hotel_id[0]->hotelId);
            if ($hotel_id[0]->hotelId != null) {
                $map_details = DB::select(DB::raw("SELECT hotel.id,hotel.hotalName, count( room.id ) AS No_of_rooms FROM room, hotel WHERE room.hotelid=hotel.id GROUP BY hotelId ;"));
                //dd($map_details);

                foreach ($map_details as $map_detail) {


                    if ($map_detail->id == $h_id) {
                        $x = $map_detail;
                        $map_details = null;
                        $map_details[0] = $map_detail;
                        break;
                        // dd($map_details);
                    }
                }
            } else {
                $map_details = DB::select(DB::raw("SELECT hotel.id,hotel.hotalName, count( room.id ) AS No_of_rooms FROM room, hotel WHERE room.hotelid = hotel.id GROUP BY hotelId;"));
            }
        } else {
            $map_details = DB::select(DB::raw("SELECT hotel.id,hotel.hotalName, count( room.id ) AS No_of_rooms FROM room, hotel WHERE room.hotelid = hotel.id GROUP BY hotelId;"));
        }


        // dd($map_details);

        return view('pages.room_details.roomMap.room_map_page', compact('map_details'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //

        $rooms = Room::where('hotelId', '=', $id)->get();


        // dd($rooms);
        // $roomsType = RoomType::where('id','=',$rooms['roomTypeId'])->get();
        foreach ($rooms as $room) {
            $roomsType = RoomType::where('id', '=', $room->roomTypeId)->pluck('typeName');
            // 
            if ($room->roomState != 'Available') {
                $reservations = Reservation::where('roomId', '=', $room->id)->get();
                $today = date("Y-m-d");
                foreach ($reservations as $reservation) {
                    //   dd($reservation->accommodateDate);
                    $accomadateDay = $reservation->accommodateDate;
                    $accommodateCloseDate = $reservation->accommodateCloseDate;
                    //dd($today <= $accommodateCloseDate);
                    if (($accomadateDay <= $today) && ($today <= $accommodateCloseDate)) {
                        //$room['roomState'] = 'Available';
                    } else if (($today < $accomadateDay) && ($today != $accomadateDay)) {
                        //do something; 
                        $room['roomState'] = 'Available';
                    } else if (($today > $accommodateCloseDate) && ($today != $accommodateCloseDate)) {
                        $room['roomState'] = 'Available';
                    } else if ($room->roomState == 'notCheckIn') {
                        //$rooms['roomState'] = 'Available';
                    }
                }
                // dd($reservation);
            }
            // dd($reservation);
            $room['roomTypeId'] = $roomsType;
        }

        //dd($rooms);
        return view('pages.room_details.roomMap.room_map_show', compact('rooms'));
        // dd($room);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function view_single_room_details($roomDetail) {
        //echo 'wada';
        //$myString = $roomDetail;
        //$myArray = explode('"', $myString);
        //dd($roomDetail);
        //  dd($roomDetail);
        $s_reservations = Reservation::where('roomId', '=', $roomDetail)->get();
        // dd('wada');


        if (count($s_reservations) == 0) {
            $s_reservations = Reservation::take(1)->get();

            $s_reservations[0]['accommodateDate'] = "Avaialble Room";
            $s_reservations[0]['accommodateCloseDate'] = "Avaialble Room";
            $s_reservations[0]['check_in'] = 'Not Check-In';
            $s_reservations[0]['check_out'] = 'Not Check-In';
            $s_reservations[0]['no_of_members'] = '0';
        } else {

            foreach ($s_reservations as $s_reservation) {
                $s_reservation['accommodateDate'] = date_create($s_reservation->accommodateDate)->format('Y-m-d');
                $s_reservation['accommodateCloseDate'] = date_create($s_reservation->accommodateCloseDate)->format('Y-m-d');


                // dd($roomCode);

                if ($s_reservation->check_in == null) {
                    $s_reservation['check_in'] = 'Not Check-In';
                    $s_reservation['check_out'] = 'Not Check-In';
                } else {
                    $s_reservation['check_in'] = date_create($s_reservation->check_in)->format('Y-m-d H:i A');
                    $s_reservation['check_out'] = date_create($s_reservation->check_out)->format('Y-m-d H:i A');
                }
            }

            // had resevation
        }

        // $room = Room::where('id', '=', $roomDetail)->get();
        $s_room_detail = DB::table('room')
                ->join('hotel', 'room.hotelId', '=', 'hotel.id')
                ->join('roomType', 'room.roomTypeId', '=', 'roomType.id')
                ->where('room.id', '=', $roomDetail)
                ->get();



        ////////////calender view//////////////////////////////////////

        $mytime = Carbon::now();
        //dd($mytime->toDateTimeString());
        $staticEvent = \Calendar::event(
                        $mytime->toDateTimeString(), true, Carbon::today()->setTime(0, 0), Carbon::today()->setTime(23, 59), null, [
                    'color' => '#800080',
                        ]
        );


        $events[] = \Calendar::event(
                        '', true, Carbon::today()->setTime(0, 0), Carbon::today()->setTime(23, 59), null, [
                    'color' => '#FCF8E3',
                        ]
        );
        //   dd($staticEvent);

        $databaseEvents = Reservation::where('roomId', '=', $roomDetail)->get();




        foreach ($databaseEvents as $databaseEvent) {

            $enddate = strtotime("+1 day", strtotime($databaseEvent->accommodateCloseDate));

            $rooms = Room::where('id', '=', $databaseEvent->roomId)->get();


            if ($rooms[0]->roomState == 'notCheckIn') {
                $events[] = \Calendar::event(
                                $rooms[0]->room_code . ' ----Customer Not Checking Yet----', //event title
                                true, //full day event?
                                new \DateTime($databaseEvent->accommodateDate), //start time (you can also use Carbon instead of DateTime)
                                new \DateTime(date("Y-m-d", $enddate)), //end time (you can also use Carbon instead of DateTime)
                                'stringEventId', //optionally, you can specify an event ID,
                                [
                            'color' => '#FFBF40',
                                ]
                );
            } else if ($rooms[0]->roomState == 'UnAvailable') {

                $events[] = \Calendar::event(
                                $rooms[0]->room_code, //event title
                                true, //full day event?
                                new \DateTime($databaseEvent->accommodateDate), //start time (you can also use Carbon instead of DateTime)
                                new \DateTime(date("Y-m-d", $enddate)), //end time (you can also use Carbon instead of DateTime)
                                'stringEventId', //optionally, you can specify an event ID,
                                [
                            'color' => '#008080', //#7EEFFB
                                ]
                );
            }
        }

        $calendar = \Calendar::addEvent($staticEvent)->addEvents($events);


        return view('pages.room_details.roomMap.single_room_map_detail', compact('s_reservations', 's_room_detail', 'calendar'));
        //dd('wada');
    }

}
