<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use DB;
use Request;
use App\Models\Reservation;
use App\Models\Room;
use App\Models\Guest;
use App\Models\Hotel;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ReservationController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //


        $reservations = Reservation::all();

        foreach ($reservations as $reservation) {
            $reservation['accommodateDate'] = date_create($reservation->accommodateDate)->format('Y-m-d');
            $reservation['accommodateCloseDate'] = date_create($reservation->accommodateCloseDate)->format('Y-m-d');

            $reservation['roomId'] = DB::table('room')->where('id', $reservation->roomId)->value('room_code');

            // dd($roomCode);

            if ($reservation->check_in == null) {
                $reservation['check_in'] = 'Not Check-In';
                $reservation['check_out'] = 'Not Check-In';
            } else {
                $reservation['check_in'] = date_create($reservation->check_in)->format('Y-m-d H:i A');
                $reservation['check_out'] = date_create($reservation->check_out)->format('Y-m-d H:i A');
            }
        }

        return view('pages.reservation_details.reservation_page', compact('reservations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
        $rooms = Room::where('roomState', '=', 'Available')->lists('room_code', 'id');
        $guests = Guest::lists('Name', 'id');
        $hotels = Hotel::lists('hotalName', 'id');

        return view('pages.reservation_details.reservation_add', compact('rooms', 'guests', 'hotels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        if (isset($_POST['reserve_room_button'])) {
            //dd('clecke_reserve_button');
            $reservations = Request::all();
            // dd($reservations);//
            list($accommodateDate, $accommodateCloseDate) = explode("-", $_POST['accom_Date']);

            $s_time = strtotime($accommodateDate);
            $s_newformat = date('Y-m-d', $s_time);

            $s2_time = strtotime($accommodateCloseDate);
            $s2_newformat = date('Y-m-d', $s2_time);

            $reservations['accommodateDate'] = $s_newformat;
            $reservations['accommodateCloseDate'] = $s2_newformat;


            if ($_POST['check_in_out'] != null) {
                list($check_in, $check_out) = explode("-", $_POST['check_in_out']);

                $check_in_time = strtotime($check_in);
                $new_check_in = date('Y-m-d H:i:s', $check_in_time);

                $check_out_time = strtotime($check_out);
                $new_check_out = date('Y-m-d H:i:s', $check_out_time);

                $reservations['check_in'] = $new_check_in;
                $reservations['check_out'] = $new_check_out;

                DB::table('room')->where('id', $_POST['roomId'])->update(['roomState' => 'UnAvailable']);
            } else {
                $reservations['check_in'] = null;
                $reservations['check_out'] = null;

                DB::table('room')->where('id', $_POST['roomId'])->update(['roomState' => 'notCheckIn']);
            }

            Reservation::create($reservations);

            return redirect('reservation_page');
        } else if (isset($_POST['register_guest_button'])) {

            //dd('click register button');
            $guests = Request::all();
            Guest::create($guests);
            //dd($_POST);

            return redirect('reservation_page');
        } else {
            //no button pressed
            return redirect('reservation_page');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //

        $reservation = Reservation::find($id);
        //2015-10-07 00:00:00-2015-10-08 00:00:00  -> 11/05/2015 - 11/05/2015
        $reservation['laccom_Date'] = date('Y/m/d', strtotime($reservation->accommodateDate)) . ' - ' . date('Y/m/d', strtotime($reservation->accommodateCloseDate));
        if ($reservation->check_in != null) {
            if ($reservation->check_out != null) {
                $reservation['lcheck_in_out'] = date('Y-m-d H:i A', strtotime($reservation->check_in)) . ' - ' . date('Y-m-d H:i A', strtotime($reservation->check_out));
            } else {
                $reservation['lcheck_in_out'] = date('Y-m-d H:i A', strtotime($reservation->check_in)) . '- Not Yet Check Out ';
            }
        } else {
            $reservation['lcheck_in_out'] = 'Not Yet Checking';
        }

        // dd($reservation);
        $rooms = Room::lists('room_code', 'id');
        $guests = Guest::lists('Name', 'id');
        $hotels = Hotel::lists('hotalName', 'id');

        return view('pages.reservation_details.reservation_update', compact('reservation', 'rooms', 'guests', 'hotels'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //

        $reservationUpdate = Request::all();
        $reservation = Reservation::find($id);


        //dd($reservationUpdate['check_in_out']);
        // dd($reservations);//
        if ($reservationUpdate['accom_Date'] != null) {
            list($accommodateDate, $accommodateCloseDate) = explode("-", $reservationUpdate['accom_Date']);
            //dd('wada');
            $s_time = strtotime($accommodateDate);
            $s_newformat = date('Y-m-d', $s_time);

            $s2_time = strtotime($accommodateCloseDate);
            $s2_newformat = date('Y-m-d', $s2_time);

            $reservationUpdate['accommodateDate'] = $s_newformat;
            $reservationUpdate['accommodateCloseDate'] = $s2_newformat;


            if ($reservationUpdate['check_in_out'] != null) {
                list($check_in, $check_out) = explode("-", $reservationUpdate['check_in_out']);


                //dd($reservationUpdate['check_in_out']);
                $check_in_time = strtotime($check_in);
                $new_check_in = date('Y-m-d H:i:s', $check_in_time);

                $check_out_time = strtotime($check_out);
                $new_check_out = date('Y-m-d H:i:s', $check_out_time);

                $reservationUpdate['check_in'] = $new_check_in;
                $reservationUpdate['check_out'] = $new_check_out;

                DB::table('room')->where('id', $reservationUpdate['roomId'])->update(['roomState' => 'UnAvailable']);
            } else {
                //dd($reservationUpdate['check_in_out']);

                $reservationUpdate['check_in'] = null;
                $reservationUpdate['check_out'] = null;

                DB::table('room')->where('id', $reservationUpdate['roomId'])->update(['roomState' => 'notCheckIn']);
            }
        }


        //
        if ($reservationUpdate['check_in_out'] == null) {
            $reservationUpdate['check_in'] = null;
            $reservationUpdate['check_out'] = null;

            DB::table('room')->where('id', $reservationUpdate['roomId'])->update(['roomState' => 'notCheckIn']);
        } else {
            list($check_in, $check_out) = explode("-", $reservationUpdate['check_in_out']);

            //dd($reservationUpdate['check_in_out']);
            $check_in_time = strtotime($check_in);
            $new_check_in = date('Y-m-d H:i:s', $check_in_time);

            $check_out_time = strtotime($check_out);
            $new_check_out = date('Y-m-d H:i:s', $check_out_time);

            $reservationUpdate['check_in'] = $new_check_in;
            $reservationUpdate['check_out'] = $new_check_out;

            DB::table('room')->where('id', $reservationUpdate['roomId'])->update(['roomState' => 'UnAvailable']);
        }



        $reservation->update($reservationUpdate);
        return redirect('reservation_page');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //

        $reservation = Reservation::find($id);
        Reservation::find($id)->delete();

        DB::table('room')->where('id', $reservation->roomId)->update(['roomState' => 'Available']);

        return redirect('reservation_page');
    }

}
