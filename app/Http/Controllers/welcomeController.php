<?php

namespace App\Http\Controllers;

use App\Models\Hotel;
use App\Models\Guest;
use App\Models\Room;
use App\Models\Reservation;


use Request;
//
//use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class welcomeController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $hotel_count = Hotel::all();
        $guess_count = Guest::all();     
        $rooms_count = Room::where('roomState', '=', 'Available')->get();
        
        
        
        $today = date("Y-m-d");
        $daily_reservation_count = Reservation::where('accommodateDate','<=',$today)->where('accommodateCloseDate','>=',$today)->get();
        //dd($daily_reservation_count);
        
        return view('pages/home',  compact('hotel_count','guess_count','daily_reservation_count','rooms_count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
