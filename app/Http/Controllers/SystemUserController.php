<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;

use Request;
use DB;
use App\Models\SystemUser;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class SystemUserController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        //$system_users = SystemUser::all();



        $system_users = DB::table('systemUser')
                        ->join('user_role', 'user_role.userId', '=', 'systemUser.id')
                        ->join('role', 'role.id', '=', 'user_role.roleId')->get();

        $rols_not_assign_users = DB::select(DB::raw("SELECT  * FROM systemUser WHERE id NOT IN (SELECT userId FROM user_role);"));

        return view('pages.user_settings.system_user_page', compact('system_users', 'rols_not_assign_users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
        //dd($_POST);
        return view('pages.user_settings.system_user_add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
        dd($_POST);
        
        SystemUser::create([
            'userName' => $_POST['userName'],
            'email' => $_POST['email'],
            'password' => bcrypt($_POST['password']),
        ]);

        return redirect('system_user_setting');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
        SystemUser::find($id)->delete();
        return redirect('system_user_setting');
    }

}
