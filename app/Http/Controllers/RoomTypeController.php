<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;

use Request;
use App\Models\RoomType;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class RoomTypeController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $roomTypes = RoomType::all();
        return view('pages.room_details.roomType.room_type_page', compact('roomTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //        
        return view('pages.room_details.roomType.room_type_add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
        $roomTypes = Request::all();
        RoomType::create($roomTypes);
        return redirect('room_type');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
        $room_type = RoomType::find($id);
        return view('pages.room_details.roomType.room_type_show', compact('room_type'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $room_type = RoomType::find($id);
        return view('pages.room_details.roomType.room_type_update', compact('room_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
        $roomTypeUpdate = Request::all();
        $roomType = RoomType::find($id);
        $roomType->update($roomTypeUpdate);
        return redirect('room_type');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
        RoomType::find($id)->delete();
        return redirect('room_type');
    }

}
