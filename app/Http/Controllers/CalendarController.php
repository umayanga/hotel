<?php

namespace App\Http\Controllers;

use App\Models\CalendarEvent;
use App\Models\Room;
use MaddHatter\LaravelFullcalendar\Event;
use Request;
//use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class CalendarController extends Controller {

    /**
     * @var CalendarEvent
     */
    private $calendarEvent;

    /**
     * @param CalendarEvent $calendarEvent
     */
    public function __construct(CalendarEvent $calendarEvent) {
        $this->calendarEvent = $calendarEvent;
        // dd($this);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function calendar() {

        $mytime = Carbon::now();
        //dd($mytime->toDateTimeString());
        $staticEvent = \Calendar::event(
                        $mytime->toDateTimeString(), true, Carbon::today()->setTime(0, 0), Carbon::today()->setTime(23, 59), null, [
                    'color' => '#800080',
                        ]
        );


        $events[] = \Calendar::event(
                        '', true, Carbon::today()->setTime(0, 0), Carbon::today()->setTime(23, 59), null, [
                    'color' => '#FCF8E3',
                        ]
        );
        //   dd($staticEvent);

        $databaseEvents = $this->calendarEvent->all();




        foreach ($databaseEvents as $databaseEvent) {

            $enddate = strtotime("+1 day", strtotime($databaseEvent->accommodateCloseDate));

            $rooms = Room::where('id', '=', $databaseEvent->roomId)->get();


            if ($rooms[0]->roomState == 'notCheckIn') {
                $events[] = \Calendar::event(
                                $rooms[0]->room_code . ' ----Customer Not Checking Yet----', //event title
                                true, //full day event?
                                new \DateTime($databaseEvent->accommodateDate), //start time (you can also use Carbon instead of DateTime)
                                new \DateTime(date("Y-m-d", $enddate)), //end time (you can also use Carbon instead of DateTime)
                                'stringEventId', //optionally, you can specify an event ID,
                                [
                            'color' => '#FFBF40',
                                ]
                );
            } else if ($rooms[0]->roomState == 'UnAvailable') {

                $events[] = \Calendar::event(
                                $rooms[0]->room_code, //event title
                                true, //full day event?
                                new \DateTime($databaseEvent->accommodateDate), //start time (you can also use Carbon instead of DateTime)
                                new \DateTime(date("Y-m-d", $enddate)), //end time (you can also use Carbon instead of DateTime)
                                'stringEventId', //optionally, you can specify an event ID,
                                [
                            'color' => '#008080', //#7EEFFB
                                ]
                );
            }
        }




        $calendar = \Calendar::addEvent($staticEvent)->addEvents($events);

        // dd($calendar);

        return view('pages.calendar.calendar', compact('calendar'));
    }

    public function av_calendar() {

        $mytime = Carbon::now();
        //dd($mytime->toDateTimeString());
        $staticEvent = \Calendar::event(
                        $mytime->toDateTimeString(), true, Carbon::today()->setTime(0, 0), Carbon::today()->setTime(23, 59), null, [
                    'color' => '#800080',
                        ]
        );


        $events[] = \Calendar::event(
                        '', true, Carbon::today()->setTime(0, 0), Carbon::today()->setTime(23, 59), null, [
                    'color' => '#FCF8E3',
                        ]
        );
        //   dd($staticEvent);
        //change to




       

        $rooms = Room::where('roomState', '=', 'Available')->get();

      //  dd($rooms);

        foreach ($rooms as $room) {
            if ($room->roomState == 'Available') {
                $events[] = \Calendar::event(
                                $room->room_code . ' ----Available ----', //event title
                                true, //full day event?
                                Carbon::today()->setTime(0, 0), //start time (you can also use Carbon instead of DateTime)
                                Carbon::today()->setTime(23, 59), //end time (you can also use Carbon instead of DateTime)
                                'stringEventId', //optionally, you can specify an event ID,
                                [
                            'color' => '#009900',
                                ]
                );
            }
        }








        $calendar = \Calendar::addEvent($staticEvent)->addEvents($events);

        // dd($calendar);

        return view('pages.reservation_details.reservation_available_calendar', compact('calendar'));
    }

}
