<?php

namespace App\Http\Controllers\Auth;

//use App\User;

use App\Models\SystemUser;
use Validator;
use Request;
use Redirect;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Registration & Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users, as well as the
      | authentication of existing users. By default, this controller uses
      | a simple trait to add these behaviors. Why don't you explore it?
      |
     */

use AuthenticatesAndRegistersUsers,
    ThrottlesLogins;

    protected $redirectPath = '/';
    protected $loginPath = 'welcome';
    private $maxLoginAttempts = 20;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {
        
      //  dd('work');
        return Validator::make($data, [
                    'userName' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:users',
                    'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data) {
        return SystemUser::create([
                    'userName' => $data['userName'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
        ]);
    }

    public function Login() {


        $rules = array(
            'email' => 'required|email', // make sure the email is an actual email
            'password' => 'required|min:6' // password can only be alphanumeric and has to be greater than 3 characters
        );

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Request::all(), $rules);

        //dd($validator->fails());
        //dd($validator);
        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::to('/')
                            ->withErrors($validator) // send back all errors to the login form
                            ->withInput(Request::except('password')); // send back the input (not the password) so that we can repopulate the form
        } else {

            //dd('ok');
            // create our user data for the authentication
            $userdata = array(
                'email' => Request::get('email'),
                'password' => Request::get('password')
            );

            // dd(Auth::attempt($userdata));
            // attempt to do the login
            if (Auth::attempt($userdata)) {
                // for now we'll just echo success (even though echoing in a controller is bad)
                //   $user = Auth::user();
                //dd($user);
                return Redirect::to('welcome');
            } else {
                // validation not successful, send back to form 
                // dd('redirect');
                return Redirect::to('/');
            }
        }
    }

}
