<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@Login');
Route::get('auth/logout', 'Auth\AuthController@getLogout');



Route::get('/', function() {
    //$user = Auth::user();
    // dd($user->name);
    if (Auth::check()) {
        // The user is logged in...
        Auth::logout();
        //echo 'some one is loged';
    }

    return view('pages/auth/login');
});




Route::group(['middleware' => 'auth'], function () {

     //dd($_POST);
    /* Route::any('welcome', function() {
      //$user = Auth::user();
      // dd($user->name);


      return view('pages/home');
      }); */

    Route::resource('welcome', 'welcomeController');

    Route::get('calendar', 'CalendarController@calendar');

    Route::get('reservation_calender', 'CalendarController@av_calendar');

    /* Route::get('calendar', function() {
      //$user = Auth::user();
      // dd($user->name);

      return view('pages/calendar/calendar');
      }); */

    Route::resource('hotel_page', 'HotelController');

    Route::resource('room_type', 'RoomTypeController');

    Route::resource('room', 'RoomController');

    Route::resource('guest_page', 'GuestController');

    Route::resource('reservation_page', 'ReservationController');


    /* Route::any('reservation_calender', function() {
      //$user = Auth::user();
      // dd($user->name);
      return view('pages/reservation_details/reservation_calender');
      }); */


    Route::resource('room_map', 'RoomMapController');


    Route::any('room_detail/{roomDetail}', 'RoomMapController@view_single_room_details');



    /* Route::any('room_detail/{roomDetail}', function($roomDetail) {

      $myString = $roomDetail;
      $myArray = explode('"', $myString);

      dd($myArray[3]);


      return view('pages/room_details/roomMap/single_room_map_detail',  compact('roomDetail'));
      }); */

    //////////sytem settings////////////////////////
    //dd($_POST);
    Route::resource('system_user_setting', 'SystemUserController');
});
