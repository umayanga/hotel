@extends('layouts.app')

@section('slide_bar')
@include('layouts.home_slide_bar')
@endsection

@section('content')




<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Welcome To Hotel<small>reserved your room</small></h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-certificate"></i> Home</a></li>
        <li class="active">Welcome page</li>
    </ol>
</section>
<br/>

<section class="content">
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>
                         {{count($rooms_count)}} </h3>
                    <p>
                        Available Room                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-calendar"></i>
                </div>
                <a href="{{url('reservation_calender')}}" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>
                        {{count($daily_reservation_count) }}                  </h3>
                    <p>
                        Today Reservations                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-home"></i>
                </div>
                <a href="{{url('reservation_page')}}" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>
                        {{ count($hotel_count)}}                    </h3>
                    <p>
                        Hotel  Details                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-clipboard"></i>
                </div>
                <a href="{{url('hotel_page')}}" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>
                        {{ count($guess_count)}}                 </h3>
                    <p>
                         Guest Details                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-stalker"></i>
                </div>
                <a href="{{url('guest_page')}}" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
    </div>

    <div class="row">
        <section class="col-lg-6 connectedSortable">   
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Number of reservations</h3>
                </div>
                <div class="box-body chart-responsive">
                    <div class="chart" id="bar-chart" style="height: 300px;"><svg style="overflow: hidden; position: relative; top: -0.433319px;" xmlns="http://www.w3.org/2000/svg" width="490" version="1.1" height="300"><desc>Created with RaphaÃ«l 2.1.0</desc><defs></defs><text font-weight="normal" font-family="sans-serif" font-size="12px" fill="#888888" stroke="none" font="10px &quot;Arial&quot;" text-anchor="end" y="260" x="20.5" style="text-anchor: end; font: 12px sans-serif;"><tspan dy="4">0</tspan></text><path stroke-width="0.5" d="M33,260H465" stroke="#aaaaaa" fill="none" style=""></path><text font-weight="normal" font-family="sans-serif" font-size="12px" fill="#888888" stroke="none" font="10px &quot;Arial&quot;" text-anchor="end" y="201.25" x="20.5" style="text-anchor: end; font: 12px sans-serif;"><tspan dy="4">2</tspan></text><path stroke-width="0.5" d="M33,201.25H465" stroke="#aaaaaa" fill="none" style=""></path><text font-weight="normal" font-family="sans-serif" font-size="12px" fill="#888888" stroke="none" font="10px &quot;Arial&quot;" text-anchor="end" y="142.5" x="20.5" style="text-anchor: end; font: 12px sans-serif;"><tspan dy="4">4</tspan></text><path stroke-width="0.5" d="M33,142.5H465" stroke="#aaaaaa" fill="none" style=""></path><text font-weight="normal" font-family="sans-serif" font-size="12px" fill="#888888" stroke="none" font="10px &quot;Arial&quot;" text-anchor="end" y="83.75" x="20.5" style="text-anchor: end; font: 12px sans-serif;"><tspan dy="4">6</tspan></text><path stroke-width="0.5" d="M33,83.75H465" stroke="#aaaaaa" fill="none" style=""></path><text font-weight="normal" font-family="sans-serif" font-size="12px" fill="#888888" stroke="none" font="10px &quot;Arial&quot;" text-anchor="end" y="25" x="20.5" style="text-anchor: end; font: 12px sans-serif;"><tspan dy="4">8</tspan></text><path stroke-width="0.5" d="M33,25H465" stroke="#aaaaaa" fill="none" style=""></path><text transform="matrix(1,0,0,1,0,7.5)" font-weight="normal" font-family="sans-serif" font-size="12px" fill="#888888" stroke="none" font="10px &quot;Arial&quot;" text-anchor="middle" y="272.5" x="447" style="text-anchor: middle; font: 12px sans-serif;"><tspan dy="4">Nov</tspan></text><text transform="matrix(1,0,0,1,0,7.5)" font-weight="normal" font-family="sans-serif" font-size="12px" fill="#888888" stroke="none" font="10px &quot;Arial&quot;" text-anchor="middle" y="272.5" x="339" style="text-anchor: middle; font: 12px sans-serif;"><tspan dy="4">Aug</tspan></text><text transform="matrix(1,0,0,1,0,7.5)" font-weight="normal" font-family="sans-serif" font-size="12px" fill="#888888" stroke="none" font="10px &quot;Arial&quot;" text-anchor="middle" y="272.5" x="267" style="text-anchor: middle; font: 12px sans-serif;"><tspan dy="4">Jun</tspan></text><text transform="matrix(1,0,0,1,0,7.5)" font-weight="normal" font-family="sans-serif" font-size="12px" fill="#888888" stroke="none" font="10px &quot;Arial&quot;" text-anchor="middle" y="272.5" x="195" style="text-anchor: middle; font: 12px sans-serif;"><tspan dy="4">Apr</tspan></text><text transform="matrix(1,0,0,1,0,7.5)" font-weight="normal" font-family="sans-serif" font-size="12px" fill="#888888" stroke="none" font="10px &quot;Arial&quot;" text-anchor="middle" y="272.5" x="123" style="text-anchor: middle; font: 12px sans-serif;"><tspan dy="4">Feb</tspan></text><text transform="matrix(1,0,0,1,0,7.5)" font-weight="normal" font-family="sans-serif" font-size="12px" fill="#888888" stroke="none" font="10px &quot;Arial&quot;" text-anchor="middle" y="272.5" x="51" style="text-anchor: middle; font: 12px sans-serif;"><tspan dy="4">Dec</tspan></text><rect fill-opacity="1" style="fill-opacity: 1;" stroke="none" fill="#00a65a" ry="0" rx="0" r="0" height="0" width="27" y="260" x="37.5"></rect><rect fill-opacity="1" style="fill-opacity: 1;" stroke="none" fill="#00a65a" ry="0" rx="0" r="0" height="0" width="27" y="260" x="73.5"></rect><rect fill-opacity="1" style="fill-opacity: 1;" stroke="none" fill="#00a65a" ry="0" rx="0" r="0" height="29.375" width="27" y="230.625" x="109.5"></rect><rect fill-opacity="1" style="fill-opacity: 1;" stroke="none" fill="#00a65a" ry="0" rx="0" r="0" height="0" width="27" y="260" x="145.5"></rect><rect fill-opacity="1" style="fill-opacity: 1;" stroke="none" fill="#00a65a" ry="0" rx="0" r="0" height="0" width="27" y="260" x="181.5"></rect><rect fill-opacity="1" style="fill-opacity: 1;" stroke="none" fill="#00a65a" ry="0" rx="0" r="0" height="0" width="27" y="260" x="217.5"></rect><rect fill-opacity="1" style="fill-opacity: 1;" stroke="none" fill="#00a65a" ry="0" rx="0" r="0" height="0" width="27" y="260" x="253.5"></rect><rect fill-opacity="1" style="fill-opacity: 1;" stroke="none" fill="#00a65a" ry="0" rx="0" r="0" height="0" width="27" y="260" x="289.5"></rect><rect fill-opacity="1" style="fill-opacity: 1;" stroke="none" fill="#00a65a" ry="0" rx="0" r="0" height="0" width="27" y="260" x="325.5"></rect><rect fill-opacity="1" style="fill-opacity: 1;" stroke="none" fill="#00a65a" ry="0" rx="0" r="0" height="58.75" width="27" y="201.25" x="361.5"></rect><rect fill-opacity="1" style="fill-opacity: 1;" stroke="none" fill="#00a65a" ry="0" rx="0" r="0" height="146.875" width="27" y="113.125" x="397.5"></rect><rect fill-opacity="1" style="fill-opacity: 1;" stroke="none" fill="#00a65a" ry="0" rx="0" r="0" height="58.75" width="27" y="201.25" x="433.5"></rect></svg><div style="left: 0px; top: 130px; display: none;" class="morris-hover morris-default-style"><div class="morris-hover-row-label">Jan</div><div class="morris-hover-point" style="color: #00a65a">
                                CPU:
                                0
                            </div></div></div>
                </div><!-- /.box-body -->
            </div>
        </section>

        <section class="col-lg-6 connectedSortable ui-sortable">
            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">People Chart</h3>
                </div>
                <div class="chart tab-pane active" id="tab-1" style="position: relative; height: 300px;">
                    <div class="box-body chart-responsive">
                        <div class="chart" id="line-chart" style="height: 250px;"><svg style="overflow: hidden; position: relative; top: -0.433319px;" xmlns="http://www.w3.org/2000/svg" width="490" version="1.1" height="250"><desc>Created with RaphaÃ«l 2.1.0</desc><defs></defs><text font-weight="normal" font-family="sans-serif" font-size="12px" fill="#888888" stroke="none" font="10px &quot;Arial&quot;" text-anchor="end" y="210" x="28.5" style="text-anchor: end; font: 12px sans-serif;"><tspan dy="4">0</tspan></text><path stroke-width="0.5" d="M41,210H465" stroke="#aaaaaa" fill="none" style=""></path><text font-weight="normal" font-family="sans-serif" font-size="12px" fill="#888888" stroke="none" font="10px &quot;Arial&quot;" text-anchor="end" y="163.75" x="28.5" style="text-anchor: end; font: 12px sans-serif;"><tspan dy="4">3</tspan></text><path stroke-width="0.5" d="M41,163.75H465" stroke="#aaaaaa" fill="none" style=""></path><text font-weight="normal" font-family="sans-serif" font-size="12px" fill="#888888" stroke="none" font="10px &quot;Arial&quot;" text-anchor="end" y="117.5" x="28.5" style="text-anchor: end; font: 12px sans-serif;"><tspan dy="4">6</tspan></text><path stroke-width="0.5" d="M41,117.5H465" stroke="#aaaaaa" fill="none" style=""></path><text font-weight="normal" font-family="sans-serif" font-size="12px" fill="#888888" stroke="none" font="10px &quot;Arial&quot;" text-anchor="end" y="71.25" x="28.5" style="text-anchor: end; font: 12px sans-serif;"><tspan dy="4">9</tspan></text><path stroke-width="0.5" d="M41,71.25H465" stroke="#aaaaaa" fill="none" style=""></path><text font-weight="normal" font-family="sans-serif" font-size="12px" fill="#888888" stroke="none" font="10px &quot;Arial&quot;" text-anchor="end" y="25" x="28.5" style="text-anchor: end; font: 12px sans-serif;"><tspan dy="4">12</tspan></text><path stroke-width="0.5" d="M41,25H465" stroke="#aaaaaa" fill="none" style=""></path><text transform="matrix(1,0,0,1,0,7.5)" font-weight="normal" font-family="sans-serif" font-size="12px" fill="#888888" stroke="none" font="10px &quot;Arial&quot;" text-anchor="middle" y="222.5" x="425.7641791044776" style="text-anchor: middle; font: 12px sans-serif;"><tspan dy="4">2015-10</tspan></text><text transform="matrix(1,0,0,1,0,7.5)" font-weight="normal" font-family="sans-serif" font-size="12px" fill="#888888" stroke="none" font="10px &quot;Arial&quot;" text-anchor="middle" y="222.5" x="348.55820895522385" style="text-anchor: middle; font: 12px sans-serif;"><tspan dy="4">2015-08</tspan></text><text transform="matrix(1,0,0,1,0,7.5)" font-weight="normal" font-family="sans-serif" font-size="12px" fill="#888888" stroke="none" font="10px &quot;Arial&quot;" text-anchor="middle" y="222.5" x="271.35223880597016" style="text-anchor: middle; font: 12px sans-serif;"><tspan dy="4">2015-06</tspan></text><text transform="matrix(1,0,0,1,0,7.5)" font-weight="normal" font-family="sans-serif" font-size="12px" fill="#888888" stroke="none" font="10px &quot;Arial&quot;" text-anchor="middle" y="222.5" x="194.14626865671642" style="text-anchor: middle; font: 12px sans-serif;"><tspan dy="4">2015-04</tspan></text><text transform="matrix(1,0,0,1,0,7.5)" font-weight="normal" font-family="sans-serif" font-size="12px" fill="#888888" stroke="none" font="10px &quot;Arial&quot;" text-anchor="middle" y="222.5" x="80.23582089552238" style="text-anchor: middle; font: 12px sans-serif;"><tspan dy="4">2015-01</tspan></text><path stroke-width="3" d="M41,210C50.808955223880595,210,70.42686567164179,210,80.23582089552238,210C90.04477611940298,210,109.66268656716417,210,119.47164179104477,210C128.33134328358207,210,146.0507462686567,210,154.910447761194,210C164.71940298507462,210,184.3373134328358,210,194.14626865671642,210C203.63880597014924,210,222.62388059701493,210,232.11641791044775,210C241.92537313432837,210,261.54328358208954,210,271.35223880597016,210C280.844776119403,210,299.8298507462687,210,309.3223880597015,210C319.13134328358205,210,338.7492537313433,210,348.55820895522385,210C358.36716417910446,210,377.98507462686564,210,387.79402985074626,210C397.2865671641791,210,416.2716417910448,210,425.7641791044776,210C435.5731343283582,210,455.1910447761194,210,465,210" stroke="#a0d0e0" fill="none" style=""></path><path stroke-width="3" d="M41,210C50.808955223880595,210,70.42686567164179,210,80.23582089552238,210C90.04477611940298,202.29166666666666,109.66268656716417,148.33333333333334,119.47164179104477,148.33333333333334C128.33134328358207,148.33333333333334,146.0507462686567,202.68361581920905,154.910447761194,210C164.71940298507462,210,184.3373134328358,210,194.14626865671642,210C203.63880597014924,210,222.62388059701493,210,232.11641791044775,210C241.92537313432837,210,261.54328358208954,210,271.35223880597016,210C280.844776119403,210,299.8298507462687,210,309.3223880597015,210C319.13134328358205,210,338.7492537313433,210,348.55820895522385,210C358.36716417910446,202.29166666666666,377.98507462686564,165.96140710382514,387.79402985074626,148.33333333333334C397.2865671641791,131.27390710382514,416.2716417910448,71.25,425.7641791044776,71.25C435.5731343283582,71.25,455.1910447761194,129.0625,465,148.33333333333334" stroke="#3c8dbc" fill="none" style=""></path><circle stroke-width="1" style="" stroke="#ffffff" fill="#a0d0e0" r="4" cy="210" cx="41"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#a0d0e0" r="4" cy="210" cx="80.23582089552238"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#a0d0e0" r="4" cy="210" cx="119.47164179104477"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#a0d0e0" r="4" cy="210" cx="154.910447761194"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#a0d0e0" r="4" cy="210" cx="194.14626865671642"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#a0d0e0" r="4" cy="210" cx="232.11641791044775"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#a0d0e0" r="4" cy="210" cx="271.35223880597016"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#a0d0e0" r="4" cy="210" cx="309.3223880597015"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#a0d0e0" r="4" cy="210" cx="348.55820895522385"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#a0d0e0" r="4" cy="210" cx="387.79402985074626"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#a0d0e0" r="4" cy="210" cx="425.7641791044776"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#a0d0e0" r="4" cy="210" cx="465"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#3c8dbc" r="4" cy="210" cx="41"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#3c8dbc" r="4" cy="210" cx="80.23582089552238"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#3c8dbc" r="4" cy="148.33333333333334" cx="119.47164179104477"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#3c8dbc" r="4" cy="210" cx="154.910447761194"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#3c8dbc" r="4" cy="210" cx="194.14626865671642"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#3c8dbc" r="4" cy="210" cx="232.11641791044775"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#3c8dbc" r="4" cy="210" cx="271.35223880597016"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#3c8dbc" r="4" cy="210" cx="309.3223880597015"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#3c8dbc" r="4" cy="210" cx="348.55820895522385"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#3c8dbc" r="4" cy="148.33333333333334" cx="387.79402985074626"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#3c8dbc" r="4" cy="71.25" cx="425.7641791044776"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#3c8dbc" r="4" cy="148.33333333333334" cx="465"></circle></svg><div style="display: none;" class="morris-hover morris-default-style"></div></div> 
                    </div> 
                </div>
            </div><!-- /.box -->
        </section>

    </div>
</section>

<script>
    // document.getElementById("slide_bars").value='trans_slide_bar'';

    var main_heder_element = document.getElementById("master_entry");
    document.getElementById("master_entry").className = "active";
</script>



@endsection