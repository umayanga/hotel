@extends('layouts.app')

@section('slide_bar')
@include('layouts.home_slide_bar')
@endsection

@section('content')




<section class="content-header">
    <h1>Add New Users <small>page </small></h1>
</section>


<br/>

<!-- Main content -->
<section class="content fluid">
    <div class="row">       
        <div class="box">
            {!! Form::open(['url' => 'system_user_setting','method' => 'POST']); !!}
            {!! Form::hidden('_token','{{ csrf_token()}}', ['class' => 'form-control', 'id' => '_token']); !!}
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-6">
                        {!! Form::label('u_name', 'User Name: '); !!}
                        {!! Form::text('userName', null, ['class' => 'form-control', 'id' => 'userName', 'placeholder' => 'User Name' ]); !!}
                    </div>
                </div>
           
                <div class="row">
                    <div class="col-sm-6">
                        {!! Form::label('u_email', 'E-Mail: '); !!}
                        {!! Form::email('email', null, ['class' => 'form-control', 'id' => 'e_mail', 'placeholder' => 'E-Mail' ]); !!}
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-6">
                        {!! Form::label('password', 'Password: '); !!} <br/>
                        {!! Form::password('password', null, ['class' => 'form-control', 'id' => 'password', 'placeholder' => 'Password' ]); !!}
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        {!! Form::label('c_password', 'Confirm Password: '); !!}<br/>
                        {!! Form::password('c_password', null, ['class' => 'form-control', 'id' => 'c_password', 'placeholder' => 'Reenter  Password Here' ]); !!}
                    </div>
                </div>
            </div><!-- /.box-body -->

            <div class="box-footer">
                <p id='buttons'>
                    {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!} &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 
                    {!! Form::reset('Cancel', ['class' => 'btn btn-default']) !!}
                </p>

            </div>
            {!! Form::close() !!}
        </div><!-- /.box -->
    </div><!-- /.row -->
</section><!-- /.content -->

<script type="text/javascript" >
    var name = document.getElementById("master_entry");
    document.getElementById("master_entry").className = "active";
    var slide_bar_element = document.getElementById("setting_menu");
    document.getElementById("setting_menu").className = "active";
    var slide_bar_element = document.getElementById("sm1_submenu");
    document.getElementById("sm1_submenu").className = "active";
</script>

@endsection

