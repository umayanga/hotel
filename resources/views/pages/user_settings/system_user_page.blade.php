@extends('layouts.app')

@section('slide_bar')
@include('layouts.home_slide_bar')
@endsection

@section('content')




<section class="content-header">
    <h1>Users  Details <small>page </small></h1>
</section>


<br/>

<!-- Main content -->
<section class="content fluid">
    <div class="row">
        <div class="box box-warning">
            <div class="gap">
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <col width='auto'>
                        <col width='auto'>
                        <col width='auto'>
                        <col width='100'>
                        <thead>
                            <tr>
                                <th>User Name</th>
                                <th>E-Mail</th>
                                <th>User Role</th>
                                <th>
                        <p id='buttons'> <a href="{{ route('system_user_setting.create') }}" class="btn btn-success"> <strong> Add New System Users &nbsp </strong> <span class="glyphicon glyphicon-plus"></span> </a> </p>
                        </th>
                        </tr>
                        </thead>
                        <tbody>                        
                            @foreach ($system_users as $system_user)

                            <tr> 
                                <td>{{ $system_user->userName }}</td>
                                <td>{{ $system_user->email }}</td>
                                <td>{{ $system_user->roll_name}}</td>
                                <td align='center'>

                                    {!! Form::open(['method' => 'DELETE', 'route'=>['system_user_setting.destroy',$system_user->id]]) !!}
                                    <a href="{{route('system_user_setting.edit',$system_user->id)}}" class="btn btn-default btn-sm"> <span class="glyphicon glyphicon-pencil"></span> </a> &nbsp &nbsp
                                    <button type="submit" class="btn btn-default btn-sm" onclick="return confirm('Are you sure?')"> <span class="glyphicon glyphicon-trash"></span> </button> 
                                    {!! Form::close() !!}

                                </td>
                            </tr>

                            @endforeach

                        </tbody>
                        <tfoot>
                            <tr>
                                <th>User Name</th>
                                <th>E-Mail</th>
                                <th>User Role</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>

                    <br/>

                    <table id="example2" class="table table-bordered table-striped">
                        <col width='auto'>
                        <col width='auto'>               
                        <col width='150'>
                        <thead>
                            <tr>
                                <th>User Name</th>
                                <th>E-Mail</th>                              
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>                        
                            @foreach ($rols_not_assign_users as $rols_not_assign_user)

                            <tr> 
                                <td>{{ $rols_not_assign_user->userName }}</td>
                                <td>{{ $rols_not_assign_user->email }}</td>                              
                                <td align='center'>

                                    {!! Form::open(['method' => 'DELETE', 'route'=>['system_user_setting.destroy',$rols_not_assign_user->id]]) !!}
                                    <a href="#" class="btn btn-default btn-sm"> <span class="glyphicon glyphicon-user"></span> </a>&nbsp &nbsp
                                    <a href="{{route('system_user_setting.edit',$rols_not_assign_user->id)}}" class="btn btn-default btn-sm"> <span class="glyphicon glyphicon-pencil"></span> </a> &nbsp &nbsp
                                    <button type="submit" class="btn btn-default btn-sm" onclick="return confirm('Are you sure?')"> <span class="glyphicon glyphicon-trash"></span> </button> 
                                    {!! Form::close() !!}

                                </td>
                            </tr>

                            @endforeach

                        </tbody>
                        <tfoot>
                            <tr>
                                <th>User Name</th>
                                <th>E-Mail</th>                          
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div><!-- /.row -->
</section><!-- /.content -->

<script type="text/javascript" >
    var name = document.getElementById("master_entry");
    document.getElementById("master_entry").className = "active";
    var slide_bar_element = document.getElementById("setting_menu");
    document.getElementById("setting_menu").className = "active";
    var slide_bar_element = document.getElementById("sm1_submenu");
    document.getElementById("sm1_submenu").className = "active";
</script>

@endsection

