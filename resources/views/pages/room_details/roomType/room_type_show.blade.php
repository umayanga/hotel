@extends('layouts.app')

@section('slide_bar')
@include('layouts.home_slide_bar')
@endsection

@section('content')



<section class="content-header">
    <h1>Hotel  Room Type View <small> page </small></h1>
</section>


<br/>
<section class='container-fluid'>
    <div class="box box-warning">
        <div class="box-body">
            {!! Form::open(['method' => 'DELETE', 'route' => ['room_type.destroy', $room_type->id]]); !!}
            <a href="javascript:history.go(-1)" class="btn btn-default" style='float: right;'> <span class="glyphicon glyphicon-remove-circle"></span> </a>
            <button type="submit" class="btn btn-default" onclick="return confirm('Are you sure?')" style='float: right;'> <span class="glyphicon glyphicon-trash"></span> </button>
            <a href="{!! url('room_type/'.$room_type->id.'/edit') !!}" class="btn btn-default" style='float: right;'> <span class="glyphicon glyphicon-pencil"></span> </a>
            {!! Form::close() !!}

            <br/> <hr/>

            <fieldset>
                <div class="row">
                    <div class="form-group">
                        <div class="col-xs-3">
                            <label>Room Type : </label>
                        </div> 
                        <div class="col-xs-9">
                            {{ $room_type->typeName }} 
                        </div>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="form-group">
                        <div class="col-xs-3">
                            <label>Room Type Description: </label>
                        </div> 
                        <div class="col-xs-9">
                            {{ $room_type->typeDescription }} 
                        </div>
                    </div>
                </div>
                
                <br/>
                
                
            </fieldset>
        </div>
    </div>
</section>


<script type="text/javascript" >
    var name = document.getElementById("master_entry");
    document.getElementById("master_entry").className = "active";
    var slide_bar_element = document.getElementById("room_menu");
    document.getElementById("room_menu").className = "active";
    var slide_bar_element = document.getElementById("rm2_submenu");
    document.getElementById("rm2_submenu").className = "active";
</script>

@endsection