@extends('layouts.app')

@section('slide_bar')
@include('layouts.home_slide_bar')
@endsection


@section('content')



<section class="content-header">
    <h1>Hotel Room Type Update <small>page </small></h1>
</section><hr/>


<section class='container-fluid'>
    <div class="box box-warning">
        <div class="box-body">

            {!! Form::model($room_type ,['method' => 'PATCH','route'=>['room_type.update',$room_type->id]]) !!}

            <div>
                <div class="form-group">
                    {!! Form::label('type_name', 'Room Type Name: '); !!}
                    <div class="form-controls">                     
                        {!! Form::text('typeName', null, ['class' => 'form-control', 'id' => 'code', 'placeholder' => 'Enter Room Type Name']); !!}
                    </div>
                </div>


                <div class="form-group">
                    {!! Form::label('type_descriptin', 'Type Description : '); !!}
                    <div class="form-controls">                     
                        {!! Form::textarea('typeDescription', null, ['class' => 'form-control', 'id' => 'typeDescription', 'placeholder' => 'Enter Room Type Description ']); !!}
                    </div>
                </div>
            </div>

            <div class="box-footer">
                <p id='buttons'>
                    {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!} &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 
                    {!! Form::reset('Cancel', ['class' => 'btn btn-default']) !!}
                </p>
                {!! Form::close() !!}
            </div>

        </div>
</section>

<script type="text/javascript" >
    var name = document.getElementById("master_entry");
    document.getElementById("master_entry").className = "active";
    var slide_bar_element = document.getElementById("room_menu");
    
    document.getElementById("room_menu").className = "active";
    
    var slide_bar_element = document.getElementById("rm2_submenu");   
    document.getElementById("rm2_submenu").className = "active";
</script>



@endsection



