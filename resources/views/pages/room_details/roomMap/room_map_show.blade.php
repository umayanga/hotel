@extends('layouts.app')

@section('slide_bar')
@include('layouts.home_slide_bar')
@endsection

@section('content')




<section class="content-header">
    <h1>Rooms Map <small>page </small></h1><br>
    <h3>Date : <?php echo date("Y-m-d H:i A") ?></h3>
</section>


<br/>

<!-- Main content -->

<section class="content">   
    <div class="box box-warning" align="center">
        <div class="gap">
            <div class="box-body" id="panel">   

                @foreach($rooms as $room)
                @if($room->roomState === 'Available')
                <div onclick="divClick('Available','{{ $room->id }}' );"  id='divelementone' style="width:150px;height:150px;border:1px solid #000;display:inline-block;background-color:#00a65a;border-radius: 5px;padding-left:10px;padding-right:10px;cursor:pointer;margin-bottom: 10px;margin-right: 10px">{{$room->room_code}}<br/> {{$room->roomState}}<br/> {{$room->roomTypeId}}</div>
                @elseif ($room->roomState === 'UnAvailable')
                <div onclick="divClick('UnAvailable', '{{ $room->id }}');" id='divelementtwo' style="width:150px;height:150px;border:1px solid #000;display:inline-block;background-color:#ed5565;border-radius: 5px;padding-left:10px;padding-right:10px;cursor:pointer;margin-bottom: 10px;margin-right: 10px">{{$room->room_code}}<br/> {{$room->roomState}}<br/> {{$room->roomTypeId}}</div>
                @elseif ($room->roomState === 'notCheckIn')
                <div onclick="divClick('notCheckIn', '{{ $room->id }}');" id='divelementthree' style="width:150px;height:150px;border:1px solid #000;display:inline-block;background-color:#FFC414;border-radius: 5px;padding-left:10px;padding-right:10px;cursor:pointer;margin-bottom: 10px;margin-right: 10px">{{$room->room_code}}<br/> {{$room->roomState}}<br/> {{$room->roomTypeId}}</div>   

                @endif
                @endforeach
            </div>
        </div>
    </div><!-- /.box --> 
</section><!-- /.content -->

<script type="text/javascript" >
                            var name = document.getElementById("master_entry");
                            document.getElementById("master_entry").className = "active";
                            var slide_bar_element = document.getElementById("room_menu");
                            document.getElementById("room_menu").className = "active";
                            var slide_bar_element = document.getElementById("rm3_submenu");
                            document.getElementById("rm3_submenu").className = "active";
                            // document.getElementById("panel").style.cursor='pointer';
</script>



<script type="text/javascript">
                                    function divClick(roomState, roomCode){
                                    //  Your code here
                                    //window.alert(roomCode);

                                    //var value_url =  roomCode;
                                    //
                                    //value_url
                                    //room_detail/{"id":"24","room_code":"H5016","roomState":"UnAvailable","hotelId":"5","roomTypeId":"7","created_at":"-0001-11-30 00:00:00","updated_at":"2015-11-04 06:52:09"}
                                    //window.alert(value_url);

                                    window.location.href = "{{url('room_detail')}}" + "/" + roomCode;
                                            //window.location.href ="{{url('room_detail/value_url')}}";

                                    }
</script>


@endsection

