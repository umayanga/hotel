@extends('layouts.app')

@section('slide_bar')
@include('layouts.home_slide_bar')
@endsection

@section('content')




<section class="content-header">
    <h1>Room Map <small>page </small></h1>
</section>


<br/>

<!-- Main content -->

<section class="content">
    <div class="row">
        <div class="box box-warning">
            <div class="gap">
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <col width='auto'> 
                        <col width='auto'> 
                        <thead>
                            <tr>
                                <th>Hotel Name</th>
                                <th>No of Rooms </th>
                            </tr>
                        </thead>
                        <tbody>                  
                            @foreach($map_details as $map_detail)
                            <tr>
                                <td><a href="{{route('room_map.show', $map_detail->id)}}"> {{ $map_detail->hotalName }} </a></td>        
                                <td><a href="#"> {{ $map_detail->No_of_rooms }} </a></td>                                   

                            </tr>
                            @endforeach

                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Hotel Name</th>
                                <th>No of Rooms </th>                            
                            </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.box -->

    </div><!-- /.row -->
</section><!-- /.content -->

<script type="text/javascript" >
    var name = document.getElementById("master_entry");
    document.getElementById("master_entry").className = "active";
    var slide_bar_element = document.getElementById("room_menu");
    document.getElementById("room_menu").className = "active";
    var slide_bar_element = document.getElementById("rm3_submenu");
    document.getElementById("rm3_submenu").className = "active";
</script>

@endsection

