@extends('layouts.app')

@section('slide_bar')
@include('layouts.home_slide_bar')
@endsection

@section('content')

<?php
//$myString = $roomDetail;
//$myArray = explode('"', $myString);
//dd($myArray);
?>

<section class="content-header">
    <h1>Room  Details View <small>page </small></h1>
</section>


<br/>

<!-- Main content -->

<section class="content">
    <div class="row">
        <div class="box box-solid box-info">
            <div class="box-header">

                {!! Form::open(['method' => 'DELETE']); !!}
                <h3 class="box-title">Room {{ $s_room_detail[0]->room_code }} Details</h3>
                <a href="javascript:history.go(-1)" class="btn btn-default" style='float: right;'> <span class="glyphicon glyphicon-remove-circle"></span> </a>                
                <a href="{!! url('room_map') !!}" class="btn btn-default" style='float: right;'> <span class="glyphicon glyphicon-pencil"></span> </a>
                {!! Form::close() !!}
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="form-group">
                        <div class="col-xs-3">
                            <label>Room Type : </label>
                        </div> 
                        <div class="col-xs-9">
                            {{ $s_room_detail[0]->typeName}} 
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <div class="col-xs-3">
                            <label>Hotel Name : </label>
                        </div> 
                        <div class="col-xs-9">
                            {{ $s_room_detail[0]->hotalName}} 
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <div class="col-xs-3">
                            <label>Hotel E-mail : </label>
                        </div> 
                        <div class="col-xs-9">
                            {{ $s_room_detail[0]->email}} 
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <div class="col-xs-3">
                            <label>Room Accommodate Date : </label>
                        </div> 
                        <div class="col-xs-9">
                            {{ $s_reservations[0]->accommodateDate}} 
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <div class="col-xs-3">
                            <label>Accommodate Close Date : </label>
                        </div> 
                        <div class="col-xs-9">
                            {{ $s_reservations[0]->accommodateCloseDate}} 
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <div class="col-xs-3">
                            <label>Check-In Date : </label>
                        </div> 
                        <div class="col-xs-9">
                            {{ $s_reservations[0]->check_in}} 
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <div class="col-xs-3">
                            <label>Check-Out Date : </label>
                        </div> 
                        <div class="col-xs-9">
                            {{ $s_reservations[0]->check_out}} 
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <div class="col-xs-3">
                            <label>No of Members : </label>
                        </div> 
                        <div class="col-xs-9">
                            {{ $s_reservations[0]->no_of_members}} 
                        </div>
                    </div>
                </div>

                <br/><hr/>

                <div class="col-xs-9">
                    <div class="box box-info">
                        <div class="form-group">           
                            <!--<div class="col-xs-9"> -->
                                {!! $calendar->calendar() !!}
                           <!-- </div>-->
                        </div>
                    </div>
                </div>




            </div>
        </div><!-- /.box -->
</section><!-- /.content -->

@section('scripts')
{!! $calendar->script() !!}
@stop


<script type="text/javascript" >
    var name = document.getElementById("master_entry");
    document.getElementById("master_entry").className = "active";
    var slide_bar_element = document.getElementById("room_menu");
    document.getElementById("room_menu").className = "active";
    var slide_bar_element = document.getElementById("rm3_submenu");
    document.getElementById("rm3_submenu").className = "active";
</script>

@endsection

