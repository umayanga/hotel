@extends('layouts.app')

@section('slide_bar')
@include('layouts.home_slide_bar')
@endsection


@section('content')

<section class="content-header">
    <h1>Hotel Add <small>page </small></h1>
</section><hr/>

<section class='container-fluid'>
    <div class="box box-warning">
        <div class="box-body">

            {!! Form::open(['url' => 'hotel_page']); !!}
            <fieldset id='myfieldset'>
                <legend> Basic Details </legend>

                <div>
                    <div class="form-group">
                        {!! Form::label('hotel_name', 'Hotel Name: '); !!}
                        <div class="form-controls">                     
                            {!! Form::text('hotalName', null, ['class' => 'form-control', 'id' => 'hotalName', 'placeholder' => 'Enter Hotel Name']); !!}
                        </div>
                    </div>


                    <div class="form-group">
                        {!! Form::label('hotel_email', 'Hotel E-mail'); !!}
                        <div class="form-controls">                     
                            {!! Form::email('email', null, ['class' => 'form-control', 'id' => 'email', 'placeholder' => 'Enter Hotel E-mail ']); !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('description', 'Hotel Description: '); !!}
                        <div class="form-controls">
                            {!! Form::textarea('hotalDescription', null, ['class' => 'form-control', 'id' => 'areafield', 'placeholder' => 'Enter Hotel Description']); !!}
                        </div>
                    </div>
                </div>
            </fieldset>
            
            <fieldset id='myfieldset'>
                <legend> Contact Details </legend>

                <div class="form-group">
                    {!! Form::label('address', 'Address 1: '); !!}
                    <div class="form-controls">
                        {!! Form::text('address', null, ['class' => 'form-control', 'id' => 'id1', 'placeholder' => 'Enter Address 1']); !!}
                    </div>
                </div>
               
                <div class='row'>

                    <div class='col-md-4'>
                        <div class="form-group">
                            {!! Form::label('telephone', 'Telephone (Office): '); !!}
                            <div class="form-controls">
                                {!! Form::text('telephone', null, ['class' => 'form-control', 'id' => 'id4', 'placeholder' => 'Office Number']); !!}
                            </div>
                        </div>
                    </div>
                    <div class='col-md-4'>
                        <div class="form-group">
                            {!! Form::label('mobile', 'Telephone (Mobile): '); !!}
                            <div class="form-controls">
                                {!! Form::text('mobile', null, ['class' => 'form-control', 'id' => 'id4', 'placeholder' => 'Mobile Number']); !!}
                            </div>
                        </div>
                    </div>
                </div>

            </fieldset> 
            
            <br>
            <div class="box-footer">
                <p id='buttons'>
                    {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!} &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 
                    {!! Form::reset('Cancel', ['class' => 'btn btn-default']) !!}
                </p>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>



<script>
    var name = document.getElementById("master_entry");
    document.getElementById("master_entry").className = "active";

    var slide_bar_element = document.getElementById("bd_menu");
    document.getElementById("bd_menu").className = "active";

    var slide_bar_element = document.getElementById("bd_submenu1");
    document.getElementById("bd_submenu1").className = "active";
</script>


@endsection

