@extends('layouts.app')

@section('slide_bar')
@include('layouts.home_slide_bar')
@endsection


@section('content')



<section class="content-header">
    <h1>Update Reservation <small>page </small></h1>
</section><hr/>


<section class='container-fluid'>
    <div class="box box-warning">
        <div class="box-body">

            {!! Form::model($reservation ,['method' => 'PATCH','route'=>['reservation_page.update',$reservation->id]]) !!}

            <fieldset id='myfieldset'>
                <!-- Date range -->
                <div class="form-group">
                    <label> Accommodate Date range:</label>
                    {!! Form::label('laccom_Date',$reservation['laccom_Date']) !!}
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" id="reservation" name="accom_Date">
                        {!! Form::hidden('accommodateDate', null) !!}
                        {!! Form::hidden('accommodateCloseDate', null) !!}

                    </div><!-- /.input group -->
                </div><!-- /.form group -->

                <!-- Date and time range -->
                <div class="form-group">
                    <label>Check-In Check-Out  range:</label>
                    {!! Form::label('lcheck_in_out',$reservation['lcheck_in_out']) !!}
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                        </div>
                        <input type="text" class="form-control pull-right" id="reservationtime" name="check_in_out">
                        {!! Form::hidden('check_in', null) !!}
                        {!! Form::hidden('check_out', null) !!}

                    </div><!-- /.input group -->
                </div><!-- /.form group -->

                <div class="form-group">
                    {!! Form::label('no_of_members', 'No of Members'); !!}
                    <div class="form-controls">                     
                        {!! Form::number('no_of_members', null, ['class' => 'form-control', 'id' => 'no_of_members', 'placeholder' => 'No: of Members ']); !!}
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6">
                            {!! Form::label('hotelId', 'Select Your Hotel: '); !!}
                            <div class="form-controls">
                                {!! Form::select('hotelId',$hotels, null, ['class' => 'form-control' ]); !!}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6">
                            {!! Form::label('roomId', 'Room NO: '); !!}
                            <div class="form-controls">
                                {!! Form::select('roomId',$rooms, null, ['class' => 'form-control' ]); !!}
                            </div>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6">
                            {!! Form::label('guessId', 'Guest Name : '); !!}
                            <div class="form-controls">
                                {!! Form::select('guestId', $guests, null, ['class' => 'form-control' ]); !!}
                            </div>
                        </div>                          
                    </div>
                </div>


            </fieldset>



            <div class="box-footer">
                <p id='buttons'>
                    {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!} &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 
                    {!! Form::reset('Cancel', ['class' => 'btn btn-default']) !!}
                </p>
                {!! Form::close() !!}
            </div>

        </div>
</section>

<script type="text/javascript" >
    var name = document.getElementById("master_entry");
    document.getElementById("master_entry").className = "active";
    var slide_bar_element = document.getElementById("m_menu");
    document.getElementById("m_menu").className = "active";
    var slide_bar_element = document.getElementById("m1_submenu");
    document.getElementById("m1_submenu").className = "active";
</script>


@endsection



