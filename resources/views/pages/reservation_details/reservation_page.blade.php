@extends('layouts.app')

@section('slide_bar')
@include('layouts.home_slide_bar')
@endsection

@section('content')




<section class="content-header">
    <h1>Reservation Details <small>page </small></h1>
</section>


<br/>

<!-- Main content -->
<section class="content fluid">
    <div class="row">
        <div class="box box-warning">
            <div class="gap">
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <col width='auto'>
                        <col width='auto'>
                        <col width='auto'>
                        <col width='auto'>
                        <col width='auto'>
                        <col width='auto'>
                        <col width='100'>

                        <thead>
                            <tr>
                                <th>Accommodate Date</th>
                                <th>Accommodate Close Date</th>
                                <th>Check In</th>
                                <th>Check Out</th>
                                <th>Members Count</th>
                                <th>Room Code</th>
                                <th><p id='buttons'> <a href="{{ route('reservation_page.create')}}" class="btn btn-success"> <strong>New Reservation &nbsp </strong> <span class="glyphicon glyphicon-plus"></span> </a> </p></th>
                        </tr>
                        </thead>
                        <tbody>                  
                            @foreach($reservations as $reservation)
                            <tr>
                                <td><a href="{{route('reservation_page.show', $reservation->id)}}"> {{ $reservation->accommodateDate }} </a></td>        
                                <td><a href="{{route('reservation_page.show', $reservation->id)}}"> {{ $reservation->accommodateCloseDate }} </a></td>        
                                <td> {{ $reservation->check_in }} </td>        
                                <td> {{ $reservation->check_out }} </td> 
                                <td> {{ $reservation->no_of_members }} </td>        
                                <td> {{ $reservation->roomId }} </td> 

                                <td align='center'>
                                    {!! Form::open(['method' => 'DELETE', 'route'=>['reservation_page.destroy',$reservation->id]]) !!}
                                    <a href="{{route('reservation_page.edit',$reservation->id)}}" class="btn btn-default btn-sm"> <span class="glyphicon glyphicon-pencil"></span> </a> &nbsp &nbsp
                                    <button type="submit" class="btn btn-default btn-sm" onclick="return confirm('Are you sure?')"> <span class="glyphicon glyphicon-trash"></span> </button> 
                                    {!! Form::close() !!}
                                </td> 
                            </tr>
                            @endforeach

                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Accommodate Date</th>
                                <th>Accommodate Close Date</th>
                                <th>Check In</th>
                                <th>Check Out</th>
                                <th>Members Count</th>
                                <th>Room Code</th>                             
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div><!-- /.row -->
</section><!-- /.content -->

<script type="text/javascript" >
    var name = document.getElementById("master_entry");
    document.getElementById("master_entry").className = "active";
    var slide_bar_element = document.getElementById("m_menu");
    document.getElementById("m_menu").className = "active";
    var slide_bar_element = document.getElementById("m1_submenu");
    document.getElementById("m1_submenu").className = "active";
</script>

@endsection

