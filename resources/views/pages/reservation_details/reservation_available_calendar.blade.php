@extends('layouts.app')

@section('slide_bar')
@include('layouts.home_slide_bar')
@endsection

@section('content')


<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="box box-primary">
            <div class="box-body no-padding">
                {!! $calendar->calendar() !!}
            </div><!-- /.box-body -->
        </div><!-- /. box -->
        <!-- </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->


@section('scripts')
{!! $calendar->script() !!}
@stop


<script type="text/javascript" >
    var name = document.getElementById("master_entry");
    document.getElementById("master_entry").className = "active";
    var slide_bar_element = document.getElementById("m_menu");
    document.getElementById("m_menu").className = "active";
    
    var slide_bar_element = document.getElementById("m3_submenu");
    document.getElementById("m3_submenu").className = "active";
</script>



@endsection