@extends('layouts.app')

@section('slide_bar')
@include('layouts.home_slide_bar')
@endsection


@section('content')

<section class="content-header">
    <h1>Guest Add <small>page </small></h1>
</section><hr/>

<section class='container-fluid'>
    <div class="box box-warning">
        <div class="box-body">

            {!! Form::open(['url' => 'guest_page']); !!}
            <fieldset id='myfieldset'>
                <legend> Basic Details </legend>

                <div>
                    <div class="form-group">
                        {!! Form::label('guest_name', 'Guest Name: '); !!}
                        <div class="form-controls">                     
                            {!! Form::text('Name', null, ['class' => 'form-control', 'id' => 'Name', 'placeholder' => 'Enter Guest Name']); !!}
                        </div>
                    </div>


                    <div class="form-group">
                        {!! Form::label('guest_nic', 'NIC :'); !!}
                        <div class="form-controls">                     
                            {!! Form::text('nic', null, ['class' => 'form-control', 'id' => 'nic', 'placeholder' => 'Enter Guest NIC ']); !!}
                        </div>
                    </div>
                </div>
            </fieldset>
            
            <fieldset id='myfieldset'>
                <legend> Contact Details </legend>

                <div class="form-group">
                    {!! Form::label('email', 'E-mail: '); !!}
                    <div class="form-controls">
                        {!! Form::email('email', null, ['class' => 'form-control', 'id' => 'id1', 'placeholder' => 'Enter Email Address ']); !!}
                    </div>
                </div>
               
                <div class='row'>
                    <div class='col-md-4'>
                        <div class="form-group">
                            {!! Form::label('telephone', 'Telephone : '); !!}
                            <div class="form-controls">
                                {!! Form::text('telephone', null, ['class' => 'form-control', 'id' => 'id4', 'placeholder' => 'Telephone ']); !!}
                            </div>
                        </div>
                    </div>
                </div>

            </fieldset> 
            
            <br>
            <div class="box-footer">
                <p id='buttons'>
                    {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!} &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 
                    {!! Form::reset('Cancel', ['class' => 'btn btn-default']) !!}
                </p>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>



<script type="text/javascript" >

    var name = document.getElementById("master_entry");
    document.getElementById("master_entry").className = "active";

    var slide_bar_element = document.getElementById("guest_menu");
    document.getElementById("guest_menu").className = "active";

</script>


@endsection

