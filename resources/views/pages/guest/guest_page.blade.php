@extends('layouts.app')

@section('slide_bar')
@include('layouts.home_slide_bar')
@endsection

@section('content')




<section class="content-header">
    <h1>Hotel Details <small>page </small></h1>
</section>


<br/>

<!-- Main content -->
<section class="content fluid">
    <div class="row">
        <div class="box box-warning">
            <div class="gap">
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <col width='auto'>
                        <col width='auto'>
                        <col width='auto'>
                        <col width='auto'>
                        <col width='100'>

                        <thead>
                            <tr>
                                <th>Guest Name</th>
                                <th>Guest Nic</th>
                                <th>Telephone</th>
                                <th>E-mail</th>
                                <th><p id='buttons'> <a href="{{ route('guest_page.create')}}" class="btn btn-success"> <strong> Add New Guest &nbsp </strong> <span class="glyphicon glyphicon-plus"></span> </a> </p></th>
                        </tr>
                        </thead>
                        <tbody>                  
                            @foreach($guests as $guest)
                                @if($guest->id != 3)
                                <tr>
                                    <td><a href="{{route('guest_page.show', $guest->id)}}"> {{ $guest->Name }} </a></td>        
                                    <td><a href="{{route('guest_page.show', $guest->id)}}"> {{ $guest->nic }} </a></td>        
                                    <td> {{ $guest->telephone }} </td>        
                                    <td> {{ $guest->email }} </td>        

                                    <td align='center'>
                                        {!! Form::open(['method' => 'DELETE', 'route'=>['guest_page.destroy',$guest->id]]) !!}
                                        <a href="{{route('guest_page.edit',$guest->id)}}" class="btn btn-default btn-sm"> <span class="glyphicon glyphicon-pencil"></span> </a> &nbsp &nbsp
                                        <button type="submit" class="btn btn-default btn-sm" onclick="return confirm('Are you sure?')"> <span class="glyphicon glyphicon-trash"></span> </button> 
                                        {!! Form::close() !!}
                                    </td> 
                                </tr>
                                @endif
                            @endforeach

                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Guest Name</th>
                                <th>Guest Nic</th>
                                <th>Telephone</th>
                                <th>E-mail</th>                             
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div><!-- /.row -->
</section><!-- /.content -->

<script type="text/javascript" >

    var name = document.getElementById("master_entry");
    document.getElementById("master_entry").className = "active";

    var slide_bar_element = document.getElementById("guest_menu");
    document.getElementById("guest_menu").className = "active";

</script>

@endsection

