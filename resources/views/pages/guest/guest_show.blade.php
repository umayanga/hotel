@extends('layouts.app')

@section('slide_bar')
@include('layouts.home_slide_bar')
@endsection

@section('content')



<section class="content-header">
    <h1>Guest  details view <small> page </small></h1>
</section>


<br/>
<section class='container-fluid'>
    <div class="box box-warning">
        <div class="box-body">
            {!! Form::open(['method' => 'DELETE', 'route' => ['guest_page.destroy', $guest->id]]); !!}
            <a href="javascript:history.go(-1)" class="btn btn-default" style='float: right;'> <span class="glyphicon glyphicon-remove-circle"></span> </a>
            <button type="submit" class="btn btn-default" onclick="return confirm('Are you sure?')" style='float: right;'> <span class="glyphicon glyphicon-trash"></span> </button>
            <a href="{!! url('guest_page/'.$guest->id.'/edit') !!}" class="btn btn-default" style='float: right;'> <span class="glyphicon glyphicon-pencil"></span> </a>
            {!! Form::close() !!}

            <br/> <hr/>

            <fieldset>
                <div class="row">
                    <div class="form-group">
                        <div class="col-xs-3">
                            <label>Guest Name : </label>
                        </div> 
                        <div class="col-xs-9">
                            {{ $guest->Name }} 
                        </div>
                    </div>
                </div>
               
                
                <br/>
                <div class="row">
                    <div class="form-group">
                        <div class="col-xs-3">
                            <label>Guest Nic: </label>
                        </div> 
                        <div class="col-xs-9">
                            {{ $guest->nic }} 
                        </div>
                    </div>
                </div>
                
                <br/>
                <div class="row">
                    <div class="form-group">
                        <div class="col-xs-3">
                            <label>Telephone: </label>
                        </div> 
                        <div class="col-xs-9">
                            {{ $guest->telephone }} 
                        </div>
                    </div>
                </div>
                
                
                
                <br/>
                <div class="row">
                    <div class="form-group">
                        <div class="col-xs-3">
                            <label>E-mail: </label>
                        </div> 
                        <div class="col-xs-9">
                            {{ $guest->email }} 
                        </div>
                    </div>
                </div>
                
                
            </fieldset>
        </div>
    </div>
</section>




<script type="text/javascript" >

    var name = document.getElementById("master_entry");
    document.getElementById("master_entry").className = "active";

    var slide_bar_element = document.getElementById("guest_menu");
    document.getElementById("guest_menu").className = "active";

</script>


@endsection