<?php

use Illuminate\Database\Seeder;
use App\Models\SystemUser as User;

class SystemUserTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
//

        //User::truncate();

        User::create([
            'userName' => 'kamal',
            'email' => 'test@seneru.com',
            'password' => Hash::make('password'),           
        ]);

        User::create([
            'userName' => 'uma',
            'email' => 'test1@seneru.com',
            'password' => Hash::make('password'),           
        ]);
        
        User::create([
            'userName' => 'sunil',
            'email' => 'test2@seneru.com',
            'password' => Hash::make('password'),         
        ]);
    }

}
