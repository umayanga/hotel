<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room', function (Blueprint $table) {
            
            $table->engine = 'InnoDB';
            $table->increments('id');
            
            $table->string('room_code',100);    
            $table->string('roomState',100);                  
            $table->integer('hotelId')->length(10)->unsigned()->nullable();
            $table->integer('roomTypeId')->length(10)->unsigned()->nullable();

            $table->timestamps();
        });
        
        Schema::table('room', function($table) {
            $table->foreign('hotelId')->references('id')->on('hotel')->onDelete('cascade');
            $table->foreign('roomTypeId')->references('id')->on('roomType')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('room');
    }
}
