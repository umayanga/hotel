<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation', function (Blueprint $table) {
            
            $table->engine = 'InnoDB';
            $table->increments('id');
                       
            $table->datetime('accommodateDate');
            $table->datetime('accommodateCloseDate');
            $table->datetime('check_in')->nullable();
            $table->datetime('check_out')->nullable();
            $table->integer('no_of_members')->nullable(); 
            
            $table->integer('roomId')->length(10)->unsigned()->nullable();
            $table->integer('guestId')->length(10)->unsigned()->nullable();

            
            $table->timestamps();
        });
        
        Schema::table('reservation', function($table) {
            $table->foreign('roomId')->references('id')->on('room')->onDelete('cascade');
            $table->foreign('guestId')->references('id')->on('guest')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reservation');
    }
}
