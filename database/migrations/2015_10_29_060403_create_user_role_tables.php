<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRoleTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('user_role', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('roleId')->length(10)->unsigned()->nullable();
            $table->integer('userId')->length(10)->unsigned()->nullable();
            $table->integer('hotelId')->length(10)->unsigned()->nullable();

            $table->timestamps();
        });


        Schema::table('user_role', function($table) {
            $table->foreign('roleId')->references('id')->on('role')->onDelete('cascade');
            $table->foreign('userId')->references('id')->on('systemUser')->onDelete('cascade');
            $table->foreign('hotelId')->references('id')->on('hotel')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('user_role');
    }

}
